# If ASHOK is available, connect to it
# If ASHOK goes away, copy the pi config and restart network
#
#!/bin/bash
set -x 
INTERFACE=wlan0
SSID=$(/sbin/iwconfig $INTERFACE 2>/dev/null | grep SSID | awk -F "\"" '{print $2}')
MAINSSID_AVAILABLE=$(sudo /sbin/iwlist wlan0 scanning | grep "MAINSSID" > /dev/null; echo $?)
BACKUPSSID_AVAILABLE=$(sudo /sbin/iwlist wlan0 scanning | grep "BACKUPSSID" > /dev/null; echo $?)
MAINSSID=0
BACKUPSSID=1
BLACKLIST_EXPIRED=1
PID=$(ps -ef | grep  "portnumber" | awk '{print $2}')

if [ -f /tmp/ssid_blacklisted ]
then
    found=$(find /tmp/ssid_blacklisted -amin +60 -amin -360 -print)
    if [ "$found" = "/tmp/ssid_blacklisted" ]
    then
	BLACKLIST_EXPIRED=1
    else
	BLACKLIST_EXPIRED=0    
    fi
fi

cleanup_ssh_tunnel () {
    if [ ! -z "$PID" ]
    then	
	kill $PID
	sleep 2
    fi
}

switch_to_backupssid () {
    /sbin/wpa_cli -i $INTERFACE disable_network $MAINSSID
    /sbin/wpa_cli -i $INTERFACE enable_network $BACKUPSSID
    /sbin/wpa_cli -i $INTERFACE select_network $BACKUPSSID
    sleep 10
}

switch_to_mainssid () {
    /sbin/wpa_cli -i $INTERFACE disable_network $BACKUPSSID
    /sbin/wpa_cli -i $INTERFACE enable_network $MAINSSID
    /sbin/wpa_cli -i $INTERFACE select_network $MAINSSID
    sleep 10
}

if [ -f /tmp/ssid_blacklisted ] && [ "$BLACKLIST_EXPIRED" -eq "0" ] && [ "$SSID" = "MAINSSID" ] && [ "$BACKUPSSID_AVAILABLE" -eq "0" ]
then  
    cleanup_ssh_tunnel
    switch_to_pi
    /data/scripts/ssh-tunnel    
    exit 0
fi

if [ "$BLACKLIST_EXPIRED" -eq "1" ]
then
    rm -rf /tmp/ssid_blacklisted
fi

if [ "$MAINSSID_AVAILABLE" -eq "0" ] && [ "$BLACKLIST_EXPIRED" -eq "1" ]
then
    if [ "$SSID" != "MAINSSID" ]
    then	
	cleanup_ssh_tunnel
	switch_to_mainssid
	/data/scripts/ssh-tunnel
    fi
   if ping -c 1 google.com -I wlan0 -4 -W 5 > /dev/null 2>&1   
   then
       rm -rf /tmp/ssid_blacklisted
       if [  -z "$PID" ]
       then
	   /data/scripts/ssh-tunnel
       fi	    
   else
       touch /tmp/ssid_blacklisted       
   fi
   exit 0
fi

if [ "$BACKUPSSID_AVAILABLE" -eq "0" ] && [ "$SSID" != "BACKUPSSID" ]
then   
    cleanup_ssh_tunnel
    switch_to_backupssid
    /data/scripts/ssh-tunnel
    exit 0
fi

if [  -z "$PID" ]
then
    /data/scripts/ssh-tunnel
fi
