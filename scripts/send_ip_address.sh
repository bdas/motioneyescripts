#!/bin/bash
set -x
LAST_IFCONFIG=/tmp/ifconfig
NEW_IFCONFIG=/tmp/ifconfig.new
IFCONFIG=$(ip -4 addr | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

# if there's no network don't do anything at all
if ! ping -c 1 google.com -4 -W 5 > /dev/null 2>&1
then
   exit 1
fi

echo $IFCONFIG > $NEW_IFCONFIG
function send_mail {
   echo "Subject: Home cam IP addresses in BBSR" > /tmp/sendmail
   cat $NEW_IFCONFIG >> /tmp/sendmail
   
   cat /tmp/sendmail | msmtp email@address.com
   rm /tmp/sendmail
   mv $NEW_IFCONFIG $LAST_IFCONFIG   
}

if [ ! -f $LAST_IFCONFIG ]
then
    send_mail
    exit
fi

if ! diff $LAST_IFCONFIG $NEW_IFCONFIG
then    
    send_mail
    exit
fi
