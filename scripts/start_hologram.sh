#!/bin/bash

# Check if we have WiFi
# If not, check if hologram is running
# If hologram is running and we don't have network
# disconnect hologram and restart
set -x
LOGNAME=check
PAGE=index.html*
retries=`cat /tmp/retry_count`
rm -rf $LOGNAME $PAGE

# if wifi is working, quit instantly
if ping -c 1 google.com -4 -W 5 > /dev/null 2>&1
then
#    kill -9 $(pgrep -f localport)
#    sleep 2
    if ! pgrep -f localport > /dev/null 2>&1
    then	
	/data/scripts/ssh-tunnel
    fi
fi

if ping -c 1 google.com -I wlan0 -4 -W 5 > /dev/null 2>&1
then
    if pgrep -f pppd && pgrep -f localport
    then
	kill -9 $(pgrep -f localport)
    fi
    sudo hologram network disconnect    	    
   exit 1
fi
   
if [ "$retries" -le 20 ]
then
    echo ""
else
    if [[ $(find /tmp/retry_count -mtime +1 -print) ]]; then
	echo 0 > /tmp/retry_count
    fi
    exit 1
fi

wget --tries=2 https://google.com -o $LOGNAME
if grep "OK" "$LOGNAME"
then
    network_ready=1
    echo 0 > /tmp/retry_count
else
    retries=$((retries+1))
    echo $retries > /tmp/retry_count
    if pgrep -f localport
    then
	kill -9 $(pgrep -f localport)
    fi    
    sudo hologram network disconnect    
    sudo hologram network connect
fi
   

       
