#!/bin/bash
#set -x
FILE=/etc/motioneye/thread-1.conf
THRESHOLD=80000
num=`/data/scripts/light_sensor.py`
lineno=$(cat $FILE | grep -Fnw "threshold"  | awk -F ":" '{print $1}')
value=$(cat $FILE | grep -nw "threshold"  | awk '{print $2}')
HIGH_THRESHOLD=5000
LOW_THRESHOLD=500
TMPFILE=/tmp/thread.tmp
restart=0
rm -rf $TMPFILE
if [ $num -lt $THRESHOLD ]
then
    if ! [ "$value" -eq "$HIGH_THRESHOLD" ]
    then
	awk 'NR=='$lineno' {$0="threshold '$HIGH_THRESHOLD'"} { print }' $FILE > $TMPFILE
	restart=1
    fi
else
    if ! [ "$value" -eq "$LOW_THRESHOLD" ]
    then
	awk 'NR=='$lineno' {$0="threshold '$LOW_THRESHOLD'"} { print }' $FILE > $TMPFILE
	restart=1
    fi
fi
if [ -f "$TMPFILE" ]
then
    if [ "$restart" -eq "1" ]
    then
	mv $TMPFILE $FILE
	service motioneye restart
	PID=$(ps -ef | grep  "[8]765" | awk '{print $2}')
	if [ ! -z "$PID" ]
	then	
	    kill $PID	    
	    sleep 2
	fi
	reboot
    fi
fi

    


