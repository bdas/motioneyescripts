#!/bin/bash
set -x
CONFFILE=/etc/motioneye/thread-1.conf
URL="http://localhost:7999/1/detection/status"
changed=0
wget -O /tmp/status $URL >/dev/null 2>&1
if cat /tmp/status | grep PAUSE
then
    MOTION_STATUS=0
else
    MOTION_STATUS=1
fi

motion_status=`cat $CONFFILE | grep -n motion_detection | awk '{print $3}'`
lineno=`cat $CONFFILE | grep -n motion_detection | awk -F ":" '{print $1}'`

if [ "$MOTION_STATUS" -eq "0" ]  && [ "$motion_status" == "on" ]
then
    awk 'NR=='$lineno' {$0="# @motion_detection off"} 1' $CONFFILE > /tmp/motion.conf
    sudo mv /tmp/motion.conf  $CONFFILE
    echo "Change file to off and restart"
    changed=1
fi

if [ "$MOTION_STATUS" -eq "1" ] && [ "$motion_status" == "off" ]
then
    awk 'NR=='$lineno' {$0="# @motion_detection on"} 1' $CONFFILE > /tmp/motion.conf
    sudo mv /tmp/motion.conf  $CONFFILE
    echo "Change file to on and restart"
    changed=1
fi

if [ "$changed" -eq "1" ]
then
    sleep 4
    reboot
fi
